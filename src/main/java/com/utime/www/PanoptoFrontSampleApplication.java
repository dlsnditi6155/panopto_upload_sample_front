package com.utime.www;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PanoptoFrontSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PanoptoFrontSampleApplication.class, args);
	}

}
